package smartHome.pet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import smartHome.eventmanagement.EventQueue;
import smartHome.pet.Dog;
import smartHome.pet.Pet;


public class PetTest {

    Pet dog;

    @Before
    public void init (){
        dog = Mockito.spy(new Dog("To the moon!"));
        EventQueue.clean();
    }

    @Test
    public void checkDesires_importantMethodCalls_getCalled(){

        // init

        dog.checkDesires();

        // verify important parameters are checked
        Mockito.verify(dog).getMaxSleep();
        Mockito.verify(dog).getMaxActivity();
        Mockito.verify(dog).getMaxHunger();
    }

    @Test
    public void checkDesires_SleepWhenTired_getsSleep(){
        //init
        int expectedSleepDesire = 5;
        Mockito.when(dog.getMaxSleep()).thenReturn(expectedSleepDesire);
        dog.setSleepDesire(expectedSleepDesire + 1);

        // act
        dog.checkDesires();

        // verify
        Assert.assertEquals(0, dog.getSleepDesire());

    }

    @Test
    public void checkDesires_AskForFood_assignedToQueue(){
        //init
        int expectedMaxHunger = 5;
        Mockito.when(dog.getMaxHunger()).thenReturn(expectedMaxHunger);
        dog.setHungerDesire(expectedMaxHunger + 1);

        // act
        dog.checkDesires();

        // verify
        Assert.assertEquals(1, EventQueue.getLength() );
    }

    @Test
    public void checkDesires_AskForActivity_assignedToQueue(){
        //init
        int expectedMaxActivity = 5;
        Mockito.when(dog.getMaxActivity()).thenReturn(expectedMaxActivity);
        dog.setActivityDesire(expectedMaxActivity + 1);

        // act
        dog.checkDesires();

        // verify
        Assert.assertEquals(1, EventQueue.getLength() );
    }
}
