package smartHome;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import smartHome.device.DeviceState;
import smartHome.device.Radiator;
import smartHome.device.coffee.CoffeeMaker;
import smartHome.device.coffee.LatteMaker;
import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.housepart.*;
import smartHome.interfaces.TimeController;
import smartHome.interfaces.TimeControllerImpl;
import smartHome.person.Child;
import smartHome.person.Father;
import smartHome.person.Mother;
import smartHome.person.Person;
import smartHome.report.Report;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProcessTests {

    House house;
    TimeController time;
    PrintStream stdout;

    private void tick(){
        time.timeTick();
    }

    @Before
    public void init(){
        HouseBuilder builder = new HouseBuilder();
        time = TimeControllerImpl.getInstance();

        stdout = System.out;
        System.setOut(new PrintStream(new OutputStream() {
            public void write(int b) {
                //DO NOTHING
            }
        }));
        house = builder                                                                             //variable house is here for better understanding what is being created
                .addAllHouseParts1()
                .addFamilyMembers1()
                .addPets1()
                .setPersonsSkills()
                .setActivityTools1()
                .getResult();
    }

    @After
    public void destruct(){
        System.setOut(stdout);
        EventQueue.clean();
        House.resetInstance();
        time.reset();
    }

    // Process test 1

    @Test
    public void FatherCanRepair_coffeeMachine_true(){
        CoffeeMaker coffeeMaker = Mockito.spy(house.getCoffeeMakers().get(0));
        Mockito.when(coffeeMaker.getCoffee()).thenReturn(1);
        Mockito.when(coffeeMaker.getWater()).thenReturn(1);
        Mockito.when(coffeeMaker.getCleanness()).thenReturn(1);
        Mockito.when(coffeeMaker.getMilk()).thenReturn(1);

        coffeeMaker.setBroken();

        // create event
        coffeeMaker.checkState();

        Father father = (Father) house.getPeople().stream().filter(t -> t instanceof Father).findFirst().get();

        Event event = EventQueue.get(father);

        father.doActivity(event);

        Assert.assertFalse(coffeeMaker.isBroken());
    }

    // Process test 2

    @Test
    public void MotherCanRepair_coffeMachine_false(){
        CoffeeMaker coffeeMaker = Mockito.spy(house.getCoffeeMakers().get(0));
        Mockito.when(coffeeMaker.getCoffee()).thenReturn(1);
        Mockito.when(coffeeMaker.getWater()).thenReturn(1);
        Mockito.when(coffeeMaker.getCleanness()).thenReturn(1);
        Mockito.when(coffeeMaker.getMilk()).thenReturn(1);

        coffeeMaker.setBroken();

        // create event
        coffeeMaker.checkState();

        Mother mother = (Mother) house.getPeople().stream().filter(t -> t instanceof Mother).findFirst().get();

        Event event = EventQueue.get(mother);

        Assert.assertNull(event);
    }

    // Process test 3

    @Test
    public void ChildCanRepair_coffeMachine_false(){
        CoffeeMaker coffeeMaker = Mockito.spy(house.getCoffeeMakers().get(0));
        Mockito.when(coffeeMaker.getCoffee()).thenReturn(1);
        Mockito.when(coffeeMaker.getWater()).thenReturn(1);
        Mockito.when(coffeeMaker.getCleanness()).thenReturn(1);
        Mockito.when(coffeeMaker.getMilk()).thenReturn(1);

        coffeeMaker.setBroken();

        // create event
        coffeeMaker.checkState();

        // get child
        Child child = (Child) house.getPeople().stream().filter(t -> t instanceof Child).findFirst().get();

        // get its possible events
        Event event = EventQueue.get(child);

        Assert.assertNull(event);
    }

    // Process test 4

    @Test
    public void Jalousie_movesDownWhenNight_true(){

        Room room = (Room) house.getHouseParts().stream().filter(r -> r instanceof Room).findAny().get();

        house.getPeople().forEach(p -> p.goToRoom(room));


        tick();


        // shall be night, windows closed, jalousies down
        Assert.assertEquals(Jalousie.jalousieState.DOWN, room.getWindow().getJalousie().getState());
        Assert.assertEquals(Window.windowState.CLOSED, room.getWindow().getState());
    }


    // Process test 5

    @Test
    public void Jalousie_movesUpWithCrowdDay_true(){

        Room room = (Room) house.getHouseParts().stream().filter(r -> r instanceof Room).findAny().get();


        for(int i = 0; i < 7; i++){
            tick();
        }

        house.getPeople().forEach(p -> p.goToRoom(room));
        tick();


        // shall be day, windows open, jalousies up
        Assert.assertEquals(Jalousie.jalousieState.UP, room.getWindow().getJalousie().getState());
        Assert.assertEquals(Window.windowState.OPEN, room.getWindow().getState());
    }

    // Process test 6

    @Test
    public void Child_sleepsInChildRoom_true(){

        List<HousePart> room = house.getHouseParts().stream().filter(r -> r.getType() == HousePartType.KIDS_ROOM1).collect(Collectors.toList());
        Child child = (Child) house.getPeople().stream().filter(t -> t instanceof Child).findFirst().get();

        // forcefully send child to sleep
        child.setSleepDesire(Integer.MAX_VALUE);
        tick();


        // verify child's sleeping habits
        Assert.assertTrue(child.getSleeping());
        Assert.assertTrue(room.contains(child.getActualRoom()));
    }

    // Process test 7

    @Test
    public void Parent_sleepsInDiningRoom_true(){

        List<HousePart> room = house.getHouseParts().stream().filter(r -> r.getType() == HousePartType.DINNING_ROOM).collect(Collectors.toList());
        Father father = (Father) house.getPeople().stream().filter(t -> t instanceof Father).findFirst().get();

        // forcefully send child to sleep
        father.setSleepDesire(Integer.MAX_VALUE);
        tick();


        // verify father's sleeping habits
        Assert.assertTrue(father.getSleeping());
        Assert.assertTrue(room.contains(father.getActualRoom()));
    }

    // Process test 8

    @Test
    public void Radiators_workAsExpected_true(){

        Room room = (Room) house.getHouseParts().stream().filter(r -> r instanceof Room).findAny().get();


        Radiator radiator = room.getRadiator();

        tick();
        int temp = room.getTemperature();
        room.checkTemperature();


        if (temp < 17) Assert.assertEquals(DeviceState.ON, radiator.getState()[0]);
        if (temp > 20) Assert.assertEquals(DeviceState.OFF, radiator.getState()[0]);

        for (int i = 0; i < 7; i++) tick();

        temp = room.getTemperature();
        room.checkTemperature();

        if (temp < 17) Assert.assertEquals(DeviceState.ON, radiator.getState()[0]);
        if (temp > 23) Assert.assertEquals(DeviceState.OFF, radiator.getState()[0]);
    }

}
