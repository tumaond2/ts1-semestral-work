package smartHome;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import smartHome.device.DeviceState;
import smartHome.device.Radiator;
import smartHome.device.coffee.CoffeeMaker;
import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.housepart.*;
import smartHome.interfaces.TimeController;
import smartHome.interfaces.TimeControllerImpl;
import smartHome.person.Child;
import smartHome.person.Father;
import smartHome.person.Mother;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Pairwise {
    public List<House> pairwiseHouseBuilder(){
        List<House> houses = new ArrayList<>();
            for (int i = 0; i < Math.pow(2, 4); i++){
                    HouseBuilder builder = new HouseBuilder();                                                                         //variable house is here for better understanding what is being created
                    builder = (0b1 & i) > 0 ? builder.addAllHouseParts1() : builder.addAllHouseParts2();
                    builder = (0b10 & i) > 0 ? builder.addFamilyMembers1() : builder.addFamilyMembers2();
                    builder = (0b100 & i) > 0 ? builder.addPets1() : builder.addPets2();
                    builder = builder.setPersonsSkills();
                    builder = (0b1000 & i) > 0 ? builder.setActivityTools1() : builder.setActivityTools2();
                    houses.add(builder.getResult());
                    House.clean();
        }
        return houses;
    }

    @Test
    public void testHousePermutationsPossible_getCreated_getCreated(){
        List<House> houses = pairwiseHouseBuilder();


        Assert.assertEquals((int) Math.pow(2, 4), houses.size());
    }
}
