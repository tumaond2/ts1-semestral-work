package smartHome.device.coffee;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import smartHome.eventmanagement.EventQueue;
import smartHome.person.Person;

public class CoffeMakerTest {
    CoffeeMaker coffeeMaker;
    Person person;

    @Before
    public void init(){
        coffeeMaker = Mockito.spy(new LatteMaker());

        Mockito.when(coffeeMaker.getCoffee()).thenReturn(1);
        Mockito.when(coffeeMaker.getWater()).thenReturn(1);
        Mockito.when(coffeeMaker.getMilk()).thenReturn(1);
        Mockito.when(coffeeMaker.getCleanness()).thenReturn(1);

        person = Mockito.mock(Person.class);
        EventQueue.clean();
    }

    @Test
    public void checkYoSelf_checksAllComponents_itDoes(){

        // act
        coffeeMaker.checkYoSelf();


        // verify
        Mockito.verify(coffeeMaker).getCoffee();
        Mockito.verify(coffeeMaker).getWater();
        Mockito.verify(coffeeMaker).getMilk();
        Mockito.verify(coffeeMaker).getCleanness();
    }

    @Test
    public void checkYoSelf_doesRefillCoffee_itDoes(){

        // init
        Mockito.when(coffeeMaker.getCoffee()).thenReturn(0);

        // act
        coffeeMaker.checkYoSelf();

        // verify
        Assert.assertEquals(1, EventQueue.getLength());
    }

    @Test
    public void checkYoSelf_doesRefillWater_itDoes(){

        // init
        Mockito.when(coffeeMaker.getWater()).thenReturn(0);

        // act
        coffeeMaker.checkYoSelf();

        // verify
        Assert.assertEquals(1, EventQueue.getLength());
    }

    @Test
    public void checkYoSelf_doesRefillMilk_itDoes(){

        // init
        Mockito.when(coffeeMaker.getMilk()).thenReturn(0);

        // act
        coffeeMaker.checkYoSelf();

        // verify
        Assert.assertEquals(1, EventQueue.getLength());
    }

    @Test
    public void checkYoSelf_doesAskForCleaning_itDoes(){

        // init
        Mockito.when(coffeeMaker.getCleanness()).thenReturn(0);

        // act
        coffeeMaker.checkYoSelf();

        // verify
        Assert.assertEquals(1, EventQueue.getLength());
    }
}
