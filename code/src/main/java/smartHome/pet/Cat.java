package smartHome.pet;

public class Cat extends Pet {

    private final static int MAX_HUNGER = 5;
    private final static int MAX_SLEEP = 8;
    private final static int MAX_ACTIVITY = 6;

    public Cat(String name) {
        super(name);
    }

    @Override
    public int getMaxHunger() {
        return MAX_HUNGER;
    }

    @Override
    public int getMaxSleep() {
        return MAX_SLEEP;
    }

    @Override
    public int getMaxActivity() {
        return MAX_ACTIVITY;
    }
}
