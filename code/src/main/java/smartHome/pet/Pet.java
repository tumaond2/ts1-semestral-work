package smartHome.pet;

import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.housepart.House;
import smartHome.housepart.HousePart;
import smartHome.housepart.HousePartType;
import smartHome.interfaces.Observer;
import smartHome.interfaces.TimeControllerImpl;
import smartHome.person.Skills;

public abstract class Pet implements Observer {

    protected int hungerDesire = 0;
    protected int sleepDesire = 0;
    protected int activityDesire = 0;
    protected String name;
    protected int time = 0;
    protected HousePart actualHousePart = null;
    protected int timeToDetach = 0;
    protected boolean waitingForPlaying = false;
    protected boolean waitingForFood = false;
    protected boolean isSleeping = false;

    public Pet(String name) {
        this.name = name;
        TimeControllerImpl.getInstance().attach(this);
        this.setActualRoom(House.getInstance().getHousePart(HousePartType.GARDEN));
    }

    /**
     * Method which is called by TimeControllerImpl every time tick.
     * Controls whole lifecycle of a pet.
     */
    @Override
    public void update(int time) {
        this.time = time;
        if (this.timeToDetach > 0) {
            timeToDetach--;
        }
        else {
            isSleeping = false;
            checkDesires();
        }
        raiseDesires();
        if (isSleeping)
            sleepDesire = 0;
    }

    /**
     * Method which checks what level are pet's desires at.
     * If a desire is above some level, new event is added to eventQueue or pet falls asleep.
     */
    public void checkDesires() {
        if (this.sleepDesire > this.getMaxSleep()) {
            System.out.println(this.getName() + " is sleeping");
            this.timeToDetach = this.timeToDetach + 4;
            this.sleepDesire = 0;
            this.isSleeping = true;
        }
        if (this.activityDesire > this.getMaxActivity() && !waitingForPlaying) {
            EventQueue.add(new Event(Event.priorities.LOW, Skills.PLAY_WITH_PET, "play with me", this, null, this.time));
            waitingForPlaying = true;
        }
        if (this.hungerDesire > this.getMaxHunger() && !waitingForFood) {
            EventQueue.add(new Event(Event.priorities.MEDIUM, Skills.FEED_PET, "feed me", this, null, this.time));
            waitingForFood = true;
        }
    }

    /**
     * Every time tick person's desires are incremented by 1.
     */
    public void raiseDesires() {
        if (this.hungerDesire < 10)
            this.hungerDesire++;
        if (this.sleepDesire < 10)
            this.sleepDesire++;
        if (this.activityDesire < 10)
            this.activityDesire++;
    }

    public String getName() { return name; }

    public HousePart getActualRoom() { return actualHousePart; }

    public void setActualRoom(HousePart housePart) { actualHousePart = housePart; }

    public abstract int getMaxHunger();
    public abstract int getMaxSleep();
    public abstract int getMaxActivity();

    public void setHungerDesire(int hungerDesire) {
        this.hungerDesire = hungerDesire;
    }

    public void setActivityDesire(int activityDesire) {
        this.activityDesire = activityDesire;
    }

    public void setWaitingForPlaying(boolean waitingForPlaying) {
        this.waitingForPlaying = waitingForPlaying;
    }

    public void setWaitingForFood(boolean waitingForFood) {
        this.waitingForFood = waitingForFood;
    }

    public int getTimeToDetach() {
        return timeToDetach;
    }

    public void setTimeToDetach(int timeToDetach) {
        this.timeToDetach = timeToDetach;
    }

    public void setSleepDesire(int sleepDesire) {this.sleepDesire = sleepDesire; }
    public int getSleepDesire() { return this.sleepDesire; }

    public int getActivityDesire() { return this.activityDesire; }
}
