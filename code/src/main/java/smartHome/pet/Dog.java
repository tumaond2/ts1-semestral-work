package smartHome.pet;

public class Dog extends Pet {

    private final static int MAX_HUNGER = 7;
    private final static int MAX_SLEEP = 9;
    private final static int MAX_ACTIVITY = 5;

    public Dog(String name) {
        super(name);
    }

    @Override
    public int getMaxHunger() {
        return MAX_HUNGER;
    }

    @Override
    public int getMaxSleep() {
        return MAX_SLEEP;
    }

    @Override
    public int getMaxActivity() {
        return MAX_ACTIVITY;
    }

}
