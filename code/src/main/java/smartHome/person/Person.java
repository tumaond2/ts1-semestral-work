package smartHome.person;

import smartHome.activitytool.ActivityTool;
import smartHome.activitytool.Bicycle;
import smartHome.activitytool.Car;
import smartHome.activitytool.GameConsole;
import smartHome.device.coffee.Coffee;
import smartHome.device.coffee.CoffeeMaker;
import smartHome.device.*;
import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.housepart.*;
import smartHome.interfaces.Observer;
import smartHome.interfaces.TimeControllerImpl;
import smartHome.pet.Pet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a bse class for its children - father, mother, child
 */
public class Person implements Observer {

    private final House house = House.getInstance();
    private final String name;
    private final HousePart sleepingHousePart;
    private HousePart actualHousePart = null;
    private boolean isSleeping = false;
    private Map<Skills, Boolean> skillsMap = new HashMap<>();
    private int activityDesire = 0;
    private int hungerDesire = 0;
    private int sleepDesire = 0;
    private int societyDesire = 0;
    private int thirstDesire = 0;
    private int timeToDetach = 0;
    private int time = 0;
    private List<Car> cars = new ArrayList<>();
    private List<Bicycle> bicycles = new ArrayList<>();
    private List<GameConsole> gameConsoles = new ArrayList<>();
    private boolean waitingForFood = false;
    private boolean waitingForCoffee = false;
    private boolean waitingToRideBike = false;
    private boolean waitingForWatchingTV = false;
    private boolean waitingForDrink = false;

    public Person(String name, HousePart sleepingHousePart) {
        this.name = name;
        this.sleepingHousePart = sleepingHousePart;
        TimeControllerImpl.getInstance().attach(this);
    }

    /**
     * @param device is a general device.
     * @return first free activity tool or @null if nothing was found.
     */
    public Device findAvailable(List<? extends Device> device) {
        for (Device d : device) {
            if (d.getState()[0] == DeviceState.OFF && d.getState()[1] == DeviceState.WELL_WORKING)
                return d;
        }
        return null;
    }

    /**
     * @param activityTools is a general term for objects of types Car, Bicycle and GameConsole.
     * @return first free activity tool or @null if nothing was found.
     */
    public ActivityTool findAvailableAT(List<? extends ActivityTool> activityTools) {
        for (ActivityTool at : activityTools) {
            if (!at.isOccupied())
                return at;
        }
        return null;
    }


    /**
     * Method which checks what level are person's desires at.
     * If a desire is above some level, new event is added to eventQueue or person falls asleep.
     */
    public void checkDesires() {
        if (this.activityDesire > 8) {
            if (findAvailableAT(bicycles) != null  && !waitingToRideBike) {
                EventQueue.add(new Event(Event.priorities.MEDIUM, Skills.RIDE_BIKE, "ride a bike", findAvailableAT(bicycles), this, time));
                waitingToRideBike = true;
            }
        }
        if (this.hungerDesire > 5) {
            if (this.skillsMap.get(Skills.PREPARE_MEAL) && !waitingForFood) {
                if (findAvailable(house.getCookers()) != null) {
                    EventQueue.add(new Event(Event.priorities.MEDIUM, Skills.PREPARE_MEAL, "prepare meal", findAvailable(house.getCookers()), this, time));
                    waitingForFood = true;
                }
            } else if (this.skillsMap.get(Skills.HEAT_FOOD) && !waitingForFood) {
                if (findAvailable(house.getMicrowaves()) != null) {
                    EventQueue.add(new Event(Event.priorities.MEDIUM, Skills.HEAT_FOOD, "heat food", findAvailable(house.getMicrowaves()), this, time));
                    waitingForFood = true;
                }
            }
        }
        if (this.thirstDesire > 6) {
            if (findAvailable(house.getFridges()) != null && !waitingForDrink) {
                EventQueue.add(new Event(Event.priorities.MEDIUM, Skills.MAKE_DRINK, "make myself a drink", findAvailable(house.getFridges()), this, time));
                waitingForDrink = true;
            }
        }
        if (this.societyDesire > 9) {
            if (findAvailable(house.getTVs()) != null && !waitingForWatchingTV) {
                EventQueue.add(new Event(Event.priorities.LOW, Skills.WATCH_TV, "watch TV", findAvailable(house.getTVs()), this, time));
                waitingForWatchingTV = true;
            }
        }
        if (this.sleepDesire > 9) {
            if ((time % 24) > 22 || (time % 24) < 6) {
                goToRoom(sleepingHousePart);
                this.timeToDetach += 6;
                this.isSleeping = true;
                System.out.println(this.getName() + " is sleeping");
            } else {
                if (findAvailable(house.getCoffeeMakers()) != null && !waitingForCoffee) {
                    EventQueue.add(new Event(Event.priorities.HIGH, Skills.MAKE_COFFEE, "must make coffee because tired", findAvailable(house.getCoffeeMakers()), this, time));
                    waitingForCoffee = true;
                }
            }
        }
    }


    /**
     * Every time tick person's desires are incremented by 1.
     */
    public void raiseDesires() {
        if (this.activityDesire < 10)
            this.activityDesire++;
        if (this.hungerDesire < 10)
            this.hungerDesire++;
        if (this.sleepDesire < 10)
            this.sleepDesire++;
        if (this.societyDesire < 10)
            this.societyDesire++;
        if (this.thirstDesire < 10)
            this.thirstDesire++;
    }

    /**
     * Method which recognizes event type by its code and calls appropriate methods or solves the problem.
     * @param e is incoming event.
     */
    public void doActivity(Event e) {
        final CoffeeMaker cm;
        final Pet pet;
        switch (e.getCode()) {
            case PLAY_WITH_PET:
                pet = (Pet) e.getObject();
                goToRoom(pet.getActualRoom());
                this.playWithPet(pet);
                break;
            case FEED_PET:
                pet = (Pet) e.getObject();
                goToRoom(pet.getActualRoom());
                this.feedPet(pet);
                break;
            case REPAIR_DEVICE:
                final Device device = (Device) e.getObject();
                goToRoom(device.getHousePart());
                System.out.println(this.getName() + " is repairing " + e.getObject().getClass().getSimpleName() + " in " + ((Device) e.getObject()).getHousePart().getType());
                device.accept(this);
                break;
            case MAKE_COFFEE:
                makeCoffee(e.getObject());
                break;
            case FILL_COFFEE:
                cm = (CoffeeMaker) e.getObject();
                goToRoom(cm.getHousePart());
                System.out.println(this.getName() + " is filling coffee");
                cm.fillCoffee();
                break;
            case FILL_WATER:
                cm = (CoffeeMaker) e.getObject();
                goToRoom(cm.getHousePart());
                System.out.println(this.getName() + " is filling water");
                cm.fillWater();
                break;
            case FILL_MILK:
                cm = (CoffeeMaker) e.getObject();
                goToRoom(cm.getHousePart());
                System.out.println(this.getName() + " is filling milk");
                cm.fillMilk();
                break;
            case CLEAN_COFFEE_MAKER:
                cm = (CoffeeMaker) e.getObject();
                goToRoom(cm.getHousePart());
                System.out.println(this.getName() + " is cleaning coffee maker");
                cm.cleanMaker();
                break;
            case RIDE_BIKE:
                final Bicycle bicycle = (Bicycle) findAvailableAT(bicycles);
                if (bicycle == null) {
                    System.out.println("no bike left for " + e.getExecutor());
                    return;
                }
                goToRoom(house.getHousePart(HousePartType.GARAGE));
                bicycle.ride(this);
                activityDesire = 0;
                timeToDetach += 2;
                break;
            case DRIVE_CAR:
                final Car car = (Car) findAvailableAT(cars);
                if (car == null)
                    return;
                goToRoom(house.getHousePart(HousePartType.GARAGE));
                final Garage garage = (Garage) House.getInstance().getHousePart(HousePartType.GARAGE);
                if (garage.getGarageDoor().getState().equals(GarageDoor.garageDoorState.CLOSED))
                    garage.getGarageDoor().open();
                garage.removePerson(this);
                this.setActualRoom(null);
                car.drive(this);
                garage.getGarageDoor().close();
                timeToDetach += 2;
                break;
            case PREPARE_MEAL:
                final Cooker cooker = (Cooker) e.getObject();
                if (cooker == null) {
                    System.out.println("No cooker to use for " + e.getExecutor().getName());
                    return;
                }
                goToRoom(cooker.getHousePart());
                cooker.turnOn();
                cooker.putFoodIn();
                cooker.attachPerson(this);
                cooker.cook(2);
                break;
            case HEAT_FOOD:
                final Microwave microwave = (Microwave) e.getObject();
                if (microwave == null) {
                    System.out.println("No microwave to use for " + e.getExecutor().getName());
                    return;
                }
                goToRoom(microwave.getHousePart());
                microwave.turnOn();
                microwave.putFoodIn();
                microwave.attachPerson(this);
                microwave.heatUp();
                break;
            case EAT:
                final Device d = (Device) e.getObject();
                goToRoom(d.getHousePart());
                takeMeal();
                break;
            case PLAY_CONSOLE:
                final GameConsole gameConsole = (GameConsole) findAvailableAT(gameConsoles);
                if (gameConsole == null)
                    return;
                goToRoom(house.getHousePart(HousePartType.LIVING_ROOM));
                gameConsole.play(this);
                timeToDetach += 2;
                break;
            case WATCH_TV:
                watchTV((TV) e.getObject());
                break;
            case MAKE_DRINK:
                final Fridge fridge = (Fridge) e.getObject();
                if (fridge == null) {
                    System.out.println("No available fridge :(");
                    return;
                }
                fridge.attachPerson(this);
                goToRoom(fridge.getHousePart());
                if (fridge.getFood()) {
                    System.out.println(this.getName() + " is drinking a cold drink from fridge.. ");
                    thirstDesire = 0;
                    waitingForDrink = false;
                    timeToDetach++;
                    fridge.detachPerson();
                } break;
            case GO_BUY_FOOD:
                goToRoom(House.getInstance().getHousePart(HousePartType.HALL));
                System.out.println(this.getName() + " is shopping");
                final Fridge f = (Fridge) e.getObject();
                f.waitToFill(1);
                break;
        }
    }

    /**
     * Method which parses time needed to read documentation of a concrete device.
     * @param device is a general device
     */
    public void readDocumentation(Device device) {
        this.timeToDetach += Integer.parseInt(device.getDocs());
    }

    /**
     * Method which is called by TimeControllerImpl every time tick.
     * Controls whole lifecycle of a person.
     * @param time is a current time tick.
     */
    @Override
    public void update(int time) {
        this.time = time;
        if (timeToDetach > 0) {
            System.out.println(this.getName() + " is still doing the activity..");
            timeToDetach--;
        } else {
            if (this.getActualRoom() == null)                                                       //Person is outside -> were driving car or riding a bike so he must come to garage first
                this.setActualRoom(House.getInstance().getHousePart(HousePartType.GARAGE));
            isSleeping = false;
            checkDesires();
            if (!isSleeping) {
                final Event e = getEvent();
                if (e != null)
                    doActivity(e);
            }
        }
        raiseDesires();
        if (isSleeping)
            sleepDesire = 0;
    }

    /**
     * Method which is called when meal is prepared for person.
     */
    public void takeMeal() {
        System.out.println(this.getName() + " is eating");
        timeToDetach++;
        waitingForFood = false;
        hungerDesire = 0;
    }

    /**
     * Visitor pattern.
     * Method which is called when a device is broken.
     * Person reads a documentation of the device.
     * @param cooker is an example of the device.
     */
    public void visitCooker(Cooker cooker) {
        readDocumentation(cooker);
        cooker.setRepaired();
    }
    public void visitMicrowave(Microwave microwave) {
        readDocumentation(microwave);
        microwave.setRepaired();
    }
    public void visitCoffeeMaker(CoffeeMaker coffeeMaker) {
        readDocumentation(coffeeMaker);
        coffeeMaker.setRepaired();
    }
    public void visitFridge(Fridge fridge) {
        readDocumentation(fridge);
        fridge.setRepaired();
    }
    public void visitLawnSprinkler(LawnSprinkler lawnSprinkler) {
        readDocumentation(lawnSprinkler);
        lawnSprinkler.setRepaired();
    }
    public void visitRadiator(Radiator radiator) {
        readDocumentation(radiator);
        radiator.setRepaired();
    }
    public void visitSmartBulb(SmartBulb smartBulb) {
        readDocumentation(smartBulb);
        smartBulb.setColor(SmartBulb.Colors.WHITE);
        smartBulb.setRepaired();
    }
    public void visitTV(TV tv) {
        readDocumentation(tv);
        tv.setRepaired();
    }
    public void visitMotionSensor(MotionSensor motionSensor) {
        readDocumentation(motionSensor);
        motionSensor.setRepaired();
    }

    /**
     * Method which is used when person wants to make a coffee.
     * @param o is a concrete coffee maker
     */
    public void makeCoffee(Object o) {
        final CoffeeMaker cm = (CoffeeMaker) o;
        goToRoom(cm.getHousePart());
        cm.turnOn();
        final Coffee coffee = cm.makeCoffee(this);
        if (coffee != null) {
            System.out.println(this.getName() + " is drinking " + coffee.getClass().getSimpleName());
            this.timeToDetach++;
            this.sleepDesire = 0;
            this.waitingForCoffee = false;
            cm.turnOff();
        } else
            System.out.println(this.getName() + " wants to drink coffee");
    }

    /**
     * Method which is used when person wants to watch TV.
     * @param tv is a concrete TV
     */
    public void watchTV(TV tv) {
        if (tv == null) {
            System.out.println("No TV to use for " + getName());
            return;
        }
        goToRoom(tv.getHousePart());
        tv.turnOn();
        if (tv.canWork()) {
            tv.watch(this);
            timeToDetach += 2;
            societyDesire = 0;
            waitingForWatchingTV = false;
        } else
            System.out.println(getName() + " wants to watch TV");
    }

    /**
     * Method which is called after pet please person to play with him/her.
     * @param pet is an general pet - cat or dog.
     */
    public void playWithPet(Pet pet) {
        this.timeToDetach += 2;
        pet.setWaitingForPlaying(false);
        pet.setTimeToDetach(pet.getTimeToDetach() + 2);
        pet.setActivityDesire(0);
        System.out.println(this.getName() + " is playing with " + pet.getName());
    }

    /**
     * Method which is called after pet please person to feed him/her.
     * @param pet is an general pet - cat or dog.
     */
    public void feedPet(Pet pet) {
        this.timeToDetach += 1;
        pet.setWaitingForFood(false);
        pet.setTimeToDetach(pet.getTimeToDetach() + 1);
        pet.setHungerDesire(0);
        System.out.println(this.getName() + " is feeding " + pet.getName());
    }

    /**
     * Method which moves person to a concrete room because of location of a device or activity tool or pet.
     * @param housePart is a general part of our house
     */
    public void goToRoom(HousePart housePart) {
        if (housePart != null) {
            final List<HousePart> path = house.getPath(this, housePart);
            if (!path.isEmpty()) {
                System.out.print(this.getName());
                for (HousePart r : path) {
                    System.out.print(" -> " + r.getType());
                    getActualRoom().removePerson(this);
                    setActualRoom(r);
                    r.addPerson(this);
                }
                System.out.println();
            }
        }
        else System.out.println(this.getName() + " doesn't know where to go");
    }

    public Map<Skills, Boolean> getSkillsMap() {
        return skillsMap;
    }

    public void setSkillsMap(Map<Skills, Boolean> map) {
        this.skillsMap = map;
    }

    private Event getEvent() {
        return EventQueue.get(this);
    }

    public String getName() {
        return name;
    }

    public HousePart getActualRoom() { return actualHousePart; }

    public boolean getSleeping() { return isSleeping; }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void setBicycles(List<Bicycle> bicycles) {
        this.bicycles = bicycles;
    }

    public void setGameConsoles(List<GameConsole> gameConsoles) {
        this.gameConsoles = gameConsoles;
    }

    public void setActualRoom(HousePart actualHousePart) { this.actualHousePart = actualHousePart; }

    public void setWaitingToRideBike(boolean waitingToRideBike) {
        this.waitingToRideBike = waitingToRideBike;
    }

    public void setSleepDesire(int sleepDesire) { this.sleepDesire = sleepDesire; }

    @Override
    public String toString() {
        return this.getName();
    }
}
