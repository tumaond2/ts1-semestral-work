package smartHome.person;

import smartHome.housepart.HousePart;

public class Mother extends Person {

    public Mother(String name, HousePart sleepingHousePart) {
        super(name, sleepingHousePart);
    }
}
