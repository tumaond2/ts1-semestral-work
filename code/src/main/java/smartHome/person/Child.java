package smartHome.person;

import smartHome.housepart.HousePart;

public class Child extends Person {

    public Child(String name, HousePart sleepingHousePart) {
        super(name, sleepingHousePart);
    }
}
