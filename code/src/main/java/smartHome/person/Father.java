package smartHome.person;

import smartHome.housepart.HousePart;

public class Father extends Person {

    public Father(String name, HousePart sleepingHousePart) {
        super(name, sleepingHousePart);
    }
}
