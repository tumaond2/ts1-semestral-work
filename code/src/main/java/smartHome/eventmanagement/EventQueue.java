package smartHome.eventmanagement;

import smartHome.person.Person;
import java.util.ArrayList;
import java.util.List;

public class EventQueue {

    private static List<Event> events = new ArrayList<>();
    private static List<Event> allEvents = new ArrayList<>();
    /**
     * Method which sorts event queue according to priorities of events.
     */
    private static void sort() {
        events.sort((o1, o2) -> Integer.compare(o2.getPriority().getLevelCode(), o1.getPriority().getLevelCode()));
    }


    /**
     * Adds a new event to queue and calls sort method.
     * @param e is the incoming event.
     */
    public static void add(Event e) {
        if (!events.contains(e)) {
            events.add(e);
            allEvents.add(e);
            sort();
        }
    }

    /**
     * Adds a new event to a concrete index in queue and calls sort method.
     * @param e is the incoming event.
     */
    public static void add(int index, Event e) {
        if (!events.contains(e)) {
            events.add(index, e);
            allEvents.add(e);
            sort();
        }
    }

    /**
     * Method which chooses an appropriate event for requesting person.
     * Appropriate means that the executor of the event is the requester or executor of event is null
     * and person must be able to do this activity.
     * @param p is an executor of an event
     * @return chosen event or null if there is no such event
     */
    public static Event get(Person p) {
        for (Event e : events) {
            //LOGGER.log(Level.INFO, e.toString());
            if (e.getExecutor() == null || e.getExecutor().equals(p)) {
                if (p.getSkillsMap().get(e.getCode())) {                                                                //Person can do this activity
                    events.remove(e);
                    return e;
                }
            }
        }
        return null;
    }

    /**
     * Method that returns all events added to EventQueue in specific time tick.
     * @param start starting index
     * @param end ending index
     * @return list of events
     */
    public static List<Event> getEventsInSpecificTime(int start, int end) {
        List<Event> list = new ArrayList<>();
        for (Event e : allEvents) {
            if (e.getTime() >= start && e.getTime() <= end) {
                list.add(e);
            }
        }
        return list;
    }

    public static int getLength(){ return events.size(); }

    public static void clean() { events.clear(); }
}
