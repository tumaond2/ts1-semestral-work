package smartHome.eventmanagement;

import smartHome.person.Person;
import smartHome.person.Skills;

public class Event {

    public enum priorities {
        LOW(0), MEDIUM(1), HIGH(2);

        private final int levelCode;

        priorities(int level) {
            this.levelCode = level;
        }

        public int getLevelCode() {
            return levelCode;
        }
    }

    private final priorities priority;
    private final Skills code;
    private final String description;
    private final Object object;
    private final Person executor;
    private final int time;

    public Event(priorities priority, Skills code, String description, Object object, Person executor, int time) {
        this.priority = priority;
        this.code = code;
        this.description = description;
        this.object = object;
        this.executor = executor;
        this.time = time;
    }

    public priorities getPriority() {
        return priority;
    }

    public Skills getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public Object getObject() {
        return object;
    }

    public Person getExecutor() {
        return executor;
    }

    public int getTime() { return time; }

    @Override
    public String toString() {
        return "Event{" +
                "priority=" + priority +
                ", code=" + code +
                ", description='" + description + '\'' +
                '}';
    }
}
