package smartHome.interfaces;

public interface TimeController {

    void attach(Observer t);

    void notifyListeners();

    void timeTick();

    void timePeriod();

    void reset();

    /*
        Instead of detach(Observer o), we use timeToDetach variable in observers
     */
}
