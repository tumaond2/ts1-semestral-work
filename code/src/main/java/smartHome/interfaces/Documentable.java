package smartHome.interfaces;

public interface Documentable {
    /**
     * Returns number as a String (because docs should be some text) which is parsed by person.
     * Person is studying docs for a time which (s)he parsed from this method.
     */
    String getDocs();
}
