package smartHome.interfaces;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class TimeControllerImpl implements TimeController {

    private List<Observer> observers = new ArrayList<>();
    private static TimeControllerImpl instance;
    private int time;
    private DecimalFormat df = new DecimalFormat("00");

    private TimeControllerImpl() { time = 0; }

    //singleton
    public static TimeControllerImpl getInstance() {
        if (instance == null)
            instance = new TimeControllerImpl();
        return instance;
    }

    @Override
    public void attach(Observer t) {
        observers.add(t);
    }

    @Override
    public void notifyListeners() {
        final List<Observer> observersCopy = List.copyOf(observers);                                                    //because during the loop new observers can be added -> error
        for (Observer t : observersCopy) {
            t.update(time);
        }
    }

    @Override
    public void timeTick() {
            System.out.println("======================================");
            System.out.println("Current time is: " + df.format(time));
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            notifyListeners();
            time++;
    }

    @Override
    public void timePeriod(){
        for(int i = 0; i < 60; i++){
            timeTick();
        }
    }

    @Override
    public void reset(){ time = 0; }

}
