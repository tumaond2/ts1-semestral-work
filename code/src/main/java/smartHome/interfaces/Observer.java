package smartHome.interfaces;

public interface Observer {

    /**
     * Method which is called on all timeListeners - devices, persons, activity tools. Controls their lifecycle.
     * @param time is current time tick
     */
    void update(int time);
}
