package smartHome.interfaces;

import smartHome.person.Person;

public interface Visitable {

    /**
     * Method that is implemented by devices and calls a visitElement method on visitor - person
     * @param person is visitor
     */
    void accept(Person person);
}
