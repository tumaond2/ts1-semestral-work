package smartHome.report;

import smartHome.activitytool.ActivityTool;
import smartHome.device.Device;
import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.housepart.House;
import smartHome.housepart.HousePart;
import smartHome.housepart.Room;
import smartHome.person.Person;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Report {

    private static Report INSTANCE;

    public synchronized static Report getInstance() {
        if (INSTANCE == null)
            INSTANCE = new Report();
        return INSTANCE;
    }


    /**
     * Generates HouseConfigurationReport.txt, where are all configurations of House made by HouseBuilder.
     */
    public void generateHouseConfigurationReport() {
        try {
            final File fileHouse = new File("HouseConfigurationReport.txt");
            //does not matter what it returns
            fileHouse.createNewFile();
            final FileWriter writer = new FileWriter(fileHouse);
            writer.write("SMART HOUSE\n\n");
            writer.write("ROOMS:\n");
            for (HousePart r : House.getInstance().getHouseParts()) {
                writer.write(r.getType().toString() + "\n");
                writer.write("- Devices: ");
                for (Device d : r.getDevices()) {
                    writer.write(d.getClass().getSimpleName() + " ");
                }
                writer.write("\n");
                if (r instanceof Room) {
                    writer.write("- Windows: " + ((Room) r).getWindow().getClass().getSimpleName() + "\n");
                    writer.write("- Jalousie: " + ((Room) r).getWindow().getJalousie().getClass().getSimpleName() + "\n");
                }
                writer.write("- Activity Tools: ");
                for (ActivityTool a : r.getActivityTools()) {
                    writer.write(a.getClass().getSimpleName() + " ");
                }
                writer.write("\n");
            }
            writer.write("\nPERSONS:\n");
            for (Person p : House.getInstance().getPeople()) {
                writer.write("- " + p.getClass().getSimpleName() + ": " + p.getName() + "\n");
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            System.out.println("Generating houseConfigurationReport failed");
            e.printStackTrace();
        }
    }

    /**
     * Generates EventReport.txt, where are all events that happened at specific time tick.
     * @param start is starting index
     * @param end is ending index
     */
    public void generateEventReport(int start, int end) {
        try {
            final File file = new File("EventReport.txt");
            file.createNewFile();
            final FileWriter writer = new FileWriter(file);
            final List<Event> eventList = EventQueue.getEventsInSpecificTime(start, end);
            writer.write("Events from tick " + start + " to tick " + end + "\n");
            writer.write("Event: priority, skill code, description, object, executor, time\n\n");
            for (Event e : eventList) {
                writer.write("Event: "+ e.getPriority() + ", " + e.getCode() + ", " + e.getDescription() + ", " + e.getObject().getClass().getSimpleName() + ", ");
                if (e.getExecutor() == null) {
                    writer.write("NONE, " );
                } else {
                    writer.write(e.getExecutor().getName() + ", " );
                }
                writer.write(e.getTime() + "\n");
            }
            writer.flush();
            writer.close();
        } catch (IOException exception) {
            System.out.println("Generating EventReport failed");
            exception.printStackTrace();
        }
    }
}
