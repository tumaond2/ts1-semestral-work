package smartHome.device;

public enum DeviceState {
    ON, OFF, BROKEN, WELL_WORKING
}
