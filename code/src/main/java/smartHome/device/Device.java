package smartHome.device;

import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.housepart.HousePart;
import smartHome.interfaces.Documentable;
import smartHome.interfaces.Observer;
import smartHome.interfaces.TimeControllerImpl;
import smartHome.interfaces.Visitable;
import smartHome.person.Person;
import smartHome.person.Skills;

import java.util.Random;

public abstract class Device implements Documentable, Visitable, Observer {

    private final DeviceState[] state = new DeviceState[2];
    private final Random random;
    private Person person;
    private HousePart housePart;
    protected int time = 0;

    public Device () {
        this.housePart = null;
        state[0] = DeviceState.OFF;
        state[1] = DeviceState.WELL_WORKING;
        random = new Random();
        TimeControllerImpl.getInstance().attach(this);
    }

    //visitor
    @Override
    public abstract void accept(Person p);

    @Override
    public abstract String getDocs();

    /**
     * Method which checks if a device is broken or working well.
     * If the device is broken, new event is added to eventQueue.
     */
    public void checkState() {
        if (this.getState()[1] == DeviceState.WELL_WORKING) {
            final int randNumber = random.nextInt(200);
            if (randNumber == 69) {
                this.setBroken();
            }
        }

        if (this.isBroken()){
            EventQueue.add(new Event(Event.priorities.MEDIUM, Skills.REPAIR_DEVICE, this.getClass().getSimpleName().toLowerCase(), this, null, time));
            System.out.println(this.getClass().getSimpleName() + " in " + this.getHousePart().getType() + " is broken");
        }
    }

    /**
     * Method which is called by TimeControllerImpl every time tick.
     * Controls whole lifecycle of a device.
     * @param time is a current time tick.
     */
    @Override
    public abstract void update(int time);

    /**
     * Method which sets a concrete person as observer of this device.
     * @param p is a concrete person.
     */
    public void attachPerson(Person p) {
        person = p;
    }

    /**
     * Method which removes current observer from an observer state.
     */
    public void detachPerson() {
        person = null;
    }

    /**
     * Method which returns current observer of this device.
     * @return is a current observer of this device.
     */
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public DeviceState[] getState() {
        return state;
    }

    public void turnOn() {
        if (state[0] == DeviceState.OFF) state[0] = DeviceState.ON;
    }

    public void turnOff() {
        if (state[0] == DeviceState.ON) state[0] = DeviceState.OFF;
    }

    public void setRepaired(){
        state[1] = DeviceState.WELL_WORKING;
    }

    public void setBroken(){
        this.state[1] = DeviceState.BROKEN;
    }

    public boolean isBroken(){
        return this.state[1] == DeviceState.BROKEN;
    }

    public boolean canWork() {
        return state[0] == DeviceState.ON && state[1] == DeviceState.WELL_WORKING;
    }

    public HousePart getHousePart() {
        return housePart;
    }

    public void setHousePart(HousePart housePart) {
        this.housePart = housePart;
    }

}
