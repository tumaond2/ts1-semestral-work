package smartHome.device;

import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.housepart.House;
import smartHome.housepart.HousePartType;
import smartHome.person.Person;
import smartHome.person.Skills;

public class Fridge extends Device {

    private int capacity;
    private int timeToWaitTo;
    private boolean waitingForFilling = false;
    private Person filler;

    public Fridge() {
        capacity = 0;
        this.setHousePart(House.getInstance().getHousePart(HousePartType.KITCHEN));
    }

    @Override
    public void accept(Person p) {
        p.visitFridge(this);
    }

    @Override
    public String getDocs() {
        return "2";
    }

    @Override
    public void update(int time) {
        this.time = time;
        checkState();
        if (time == timeToWaitTo && waitingForFilling)
            fillFridge();
    }

    public void waitToFill(int timeOfComing) {
        timeToWaitTo = time + timeOfComing;
    }

    public boolean getFood() {
        if (capacity > 0) {
            capacity--;
            return true;
        }
        else {
            if (!waitingForFilling) {
                System.out.println("Fridge empty, must go shopping :(");
                EventQueue.add(0, new Event(Event.priorities.HIGH, Skills.GO_BUY_FOOD, "go buy some food", this, getPerson(), time));
                filler = getPerson();
                waitingForFilling = true;
            } else
                System.out.println("Fridge empty, let's wait till somebody comes back from shop");

            EventQueue.add(1, new Event(Event.priorities.HIGH, Skills.MAKE_DRINK, "make myself a drink", this, getPerson(), time));
            return false;
        }
    }

    public void fillFridge(){
        capacity = 50;
        waitingForFilling = false;
        System.out.println(filler.getName() + " filled fridge!");
    }

}
