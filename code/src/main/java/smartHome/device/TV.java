package smartHome.device;

import smartHome.housepart.House;
import smartHome.housepart.HousePartType;
import smartHome.person.Child;
import smartHome.person.Father;
import smartHome.person.Mother;
import smartHome.person.Person;

public class TV extends Device {

    public enum channels{
        tvBarrandov, kinoBarrandov, krimiBarrandov, newsBarrandov
    }

    private channels channel;
    private int timeFrom = 0;

    public TV (){
        channel = channels.tvBarrandov;
        this.setHousePart(House.getInstance().getHousePart(HousePartType.LIVING_ROOM));
    }

    @Override
    public void accept(Person p) {
        p.visitTV(this);
    }

    @Override
    public String getDocs() {
        return "3";
    }

    @Override
    public void update(int time) {
        this.time = time;
        checkState();
        if (time == timeFrom + 2) turnOff();
    }

    public void setChannel(channels channel){
        this.channel = channel;
    }

    /**
     * Method which changes a channel up.
     */
    public void setChannelUp(){
        switch (channel){
            case tvBarrandov: channel = channels.kinoBarrandov; break;
            case kinoBarrandov: channel = channels.krimiBarrandov; break;
            case krimiBarrandov: channel = channels.newsBarrandov; break;
            default:
        }
    }

    /**
     * Method which changes a channel down.
     */
    public void setChannelDown(){
        switch (channel){
            case newsBarrandov: channel = channels.krimiBarrandov; break;
            case krimiBarrandov: channel = channels.kinoBarrandov; break;
            case kinoBarrandov: channel = channels.tvBarrandov; break;
            default:
        }
    }


    /**
     * Method which sets channel depending on type of person.
     * @param person Father/Mother/Child
     */
    public void watch(Person person){
            if (person instanceof Father) {
                setChannel(channels.krimiBarrandov);
            } else if (person instanceof Mother) {
                setChannel(channels.tvBarrandov);
            } else if (person instanceof Child) {
                setChannel(channels.kinoBarrandov);
            }
            System.out.println(person.getName() + " is watching " + channel.toString());
            timeFrom = time;
    }
}
