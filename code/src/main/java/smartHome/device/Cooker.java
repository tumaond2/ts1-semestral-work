package smartHome.device;

import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.housepart.House;
import smartHome.housepart.HousePartType;
import smartHome.person.Person;
import smartHome.person.Skills;

public class Cooker extends Device {

    private boolean foodInside;
    private int timeCookingFrom;
    private int timeToCook;

    public Cooker () {
        foodInside = false;
        this.setHousePart(House.getInstance().getHousePart(HousePartType.KITCHEN));
    }

    @Override
    public void accept(Person p) {
        p.visitCooker(this);
    }

    @Override
    public String getDocs() {
        return "2";
    }

    @Override
    public void update(int time) {
        this.time = time;
        checkState();
        checkMeal();
    }

    public void notifyPerson() {
        //this is not really observer but it actually acts like that. If we call directly method on Person, it would cause concurrent activities of person in 1 time slot
        EventQueue.add(0, new Event(Event.priorities.HIGH, Skills.EAT, "meal is done!", this, getPerson(), this.time));
    }

    public void takeFoodOut(){
        foodInside = false;
    }

    public void putFoodIn(){
        foodInside = true;
    }

    public boolean canCook() {
        return foodInside && canWork();
    }

    public void checkMeal() {
        if (this.getState()[0] == DeviceState.ON) {
            if (time == timeCookingFrom + timeToCook) {
                notifyPerson();
                this.turnOff();
                this.takeFoodOut();
                System.out.println("Meal for " + getPerson().getName() + " is done.");
                this.detachPerson();
            }
        }
    }

    public void cook(int time) {
        if (canCook()) {
            timeToCook = time;
            timeCookingFrom = this.time + 1;            //devices are updated after persons so they work with "obsolete" time for persons
            System.out.println(getPerson().getName() + " is cooking");
        }
    }
}
