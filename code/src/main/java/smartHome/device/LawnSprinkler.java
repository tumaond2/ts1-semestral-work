package smartHome.device;

import smartHome.person.Person;

public class LawnSprinkler extends Device {

    @Override
    public void accept(Person p) {
        p.visitLawnSprinkler(this);
    }

    @Override
    public String getDocs() {
        return "1";
    }

    @Override
    public void update(int time) {
        this.time = time;
        checkState();
    }


}
