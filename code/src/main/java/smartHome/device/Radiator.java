package smartHome.device;

import smartHome.person.Person;

public class Radiator extends Device {
    public Radiator() {

    }

    @Override
    public void accept(Person p) {
        p.visitRadiator(this);
    }

    @Override
    public String getDocs() {
        return "3";
    }


    @Override
    public void update(int time) {
        this.time = time;
        checkState();
    }
}
