package smartHome.device;

import smartHome.person.Person;

public class MotionSensor extends Device {

    private boolean active;

    public MotionSensor() {
        active = false;
    }

    @Override
    public void accept(Person p) {
        p.visitMotionSensor(this);
    }

    @Override
    public String getDocs() {
        return "2";
    }


    @Override
    public void update(int time) {
        this.time = time;
        checkState();
    }

    /**
     * Method which returns true if this device is working well
     * If it is broken it returns always bad state.
     * @return boolean
     */
    public boolean isActive() {
        if (getState()[1] == DeviceState.WELL_WORKING) {
            return active;
        }
        return !active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
