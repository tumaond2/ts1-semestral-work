package smartHome.device;


import smartHome.person.Person;

public class SmartBulb extends Device {

    public enum Colors {
        WHITE, YELLOW
    }

    private Colors color;

    public SmartBulb (){
        color = Colors.WHITE;
    }

    @Override
    public void accept(Person p) {
        p.visitSmartBulb(this);
    }

    @Override
    public String getDocs() {
        return "1";
    }

    @Override
    public void update(int time) {
        this.time = time;
        checkState();
        if (getState()[1] == DeviceState.BROKEN)
            this.setColor(Colors.YELLOW);
    }

    public void setColor(Colors color){
        this.color = color;
    }

    public Colors getColor() {
        return color;
    }
}
