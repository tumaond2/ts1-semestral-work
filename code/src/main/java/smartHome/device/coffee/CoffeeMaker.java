package smartHome.device.coffee;

import smartHome.device.Device;
import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.housepart.House;
import smartHome.housepart.HousePartType;
import smartHome.person.Person;
import smartHome.person.Skills;

public abstract class CoffeeMaker extends Device {

    private final int coffeeMax = 10; // number of coffee, water and milk level
    private final int cleannessMax = 25;
    private int coffee;
    private int water;
    private int milk;
    private int cleanness;

    public CoffeeMaker() {
        coffee = 0;
        water = 0;
        milk = 0;
        cleanness = cleannessMax;
        setHousePart(House.getInstance().getHousePart(HousePartType.KITCHEN));
    }

    /**
     * This method checks if there is a need to fill some resources - milk, water, coffee - or clean coffeemaker
     * after is was used to make a coffee.
     */
    public void checkYoSelf() {
        if (this.getCoffee() == 0)
            EventQueue.add(new Event(Event.priorities.LOW, Skills.FILL_COFFEE, "Not enough coffee", this, null, time));
        if (this.getWater() == 0)
            EventQueue.add(new Event(Event.priorities.LOW, Skills.FILL_WATER, "Not enough water", this, null, time));
        if (this.getMilk() == 0)
            EventQueue.add(new Event(Event.priorities.LOW, Skills.FILL_MILK, "Not enough milk", this, null, time));
        if (this.getCleanness() < 1)
            EventQueue.add(new Event(Event.priorities.LOW, Skills.CLEAN_COFFEE_MAKER, "Coffee maker is dirty", this, null, time));
    }

    /**
     * Method which finds out if there is enough resources to make a concrete coffee
     * and if a coffeemaker is in workable state.
     * @param requester is a person who wants to make a coffee
     * @return if it is possible to make coffee or not
     */
    abstract boolean canMakeCoffee(Person requester);

    /**
     * Method which returns a newly created concrete coffee and decrement its resources.
     * @return new created coffee
     */
    abstract Coffee make();

    // template method
    /**
     * This is a template method which says in which order commands should be done.
     * @param requester is a person who wants to drink coffee
     * @return created coffee
     */
    public Coffee makeCoffee(Person requester) {
        if (canMakeCoffee(requester) && canWork()) {
            final Coffee coffeeToReturn = make();
            checkYoSelf();
            return coffeeToReturn;
        }
        return null;
    }

    public int getCoffee() {
        return coffee;
    }

    public int getWater() {
        return water;
    }

    public int getMilk() {
        return milk;
    }

    public int getCleanness() {
        return cleanness;
    }

    public void fillCoffee() { this.coffee = coffeeMax; }

    public void fillWater() { this.water = coffeeMax; }

    public void fillMilk() { this.milk = coffeeMax; }

    public void cleanMaker() {this.cleanness = cleannessMax; }

    public void setCoffee(int coffee) {
        this.coffee = coffee;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }

    public void setCleanness(int cleanness) {
        this.cleanness = cleanness;
    }
}
