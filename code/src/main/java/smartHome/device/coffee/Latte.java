package smartHome.device.coffee;

public class Latte extends Coffee {

    static final int COFFEE_REQ = 1;
    static final int WATER_REQ = 1;
    static final int MILK_REQ = 2;
}
