package smartHome.device.coffee;

import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.person.Person;
import smartHome.person.Skills;

public class TurekMaker extends CoffeeMaker {

    private int missingThingsNumber = 0;

    @Override
    boolean canMakeCoffee(Person requester) {
        if (this.getCoffee() < Turek.COFFEE_REQ) {
            EventQueue.add(missingThingsNumber, new Event(Event.priorities.HIGH, Skills.FILL_COFFEE, "Not enough coffee", this, requester, time));
            missingThingsNumber++;
        }
        if (this.getWater() < Turek.WATER_REQ) {
            EventQueue.add(missingThingsNumber, new Event(Event.priorities.HIGH, Skills.FILL_WATER, "Not enough water", this, requester, time));
            missingThingsNumber++;
        }
        if (this.getCleanness() < 1) {
            EventQueue.add(missingThingsNumber, new Event(Event.priorities.HIGH, Skills.CLEAN_COFFEE_MAKER, "Coffee maker is dirty", this, requester, time));
            missingThingsNumber++;
        }
        if (missingThingsNumber > 0) {
            EventQueue.add(missingThingsNumber, new Event(Event.priorities.HIGH, Skills.MAKE_COFFEE, "turek", this, requester, time));
            missingThingsNumber = 0;
            return false;
        }
        return true;
    }

    @Override
    Coffee make() {
        this.setCoffee(this.getCoffee() - Turek.COFFEE_REQ);
        this.setWater(this.getWater() - Turek.WATER_REQ);
        this.setCleanness(this.getCleanness() - 1);
        return new Turek();
    }

    @Override
    public void accept(Person p) {
        p.visitCoffeeMaker(this);
    }

    @Override
    public String getDocs() {
        return "1";
    }

    @Override
    public void update(int time) {
        this.time = time;
        checkState();
    }
}
