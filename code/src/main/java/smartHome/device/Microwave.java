package smartHome.device;

import smartHome.eventmanagement.Event;
import smartHome.eventmanagement.EventQueue;
import smartHome.housepart.House;
import smartHome.housepart.HousePartType;
import smartHome.person.Person;
import smartHome.person.Skills;

public class Microwave extends Device {

    private boolean foodInside;

    public Microwave (){
        foodInside = false;
        this.setHousePart(House.getInstance().getHousePart(HousePartType.KITCHEN));
    }

    @Override
    public void accept(Person p) {
        p.visitMicrowave(this);
    }

    @Override
    public String getDocs() {
        return "2";
    }

    @Override
    public void update(int time) {
        this.time = time;
        checkState();
    }

    public void takeFoodOut(){
        foodInside = false;
    }

    public void putFoodIn(){
        foodInside = true;
    }

    public void notifyPerson() {
        //this is not really observer but it actually acts like that. If we call directly method on Person, it would cause concurrent activities of person in 1 time slot
        EventQueue.add(0, new Event(Event.priorities.HIGH, Skills.EAT, "meal is done!", this, getPerson(), time));
        takeFoodOut();
    }


    public void heatUp() {
        if (canWork() && foodInside) {
            System.out.println(getPerson().getName() + " is heating meal in microwave");
            notifyPerson();
        }
    }
}
