package smartHome.housepart;

import smartHome.device.Radiator;
import smartHome.person.Person;

public class Room extends HousePart{
    private int temperature;
    private Radiator radiator;
    private Window window;

    public Room(HousePartType type) {
        super(type);
        this.radiator = new Radiator();
        this.window = new Window();
        radiator.setHousePart(this);
        window.setHousePart(this);
        window.setJalousie(new Jalousie());
        window.getJalousie().setWindow(window);
        devices.add(radiator);
        House.getInstance().addRadiator(radiator);
    }

    /**
     * Method which checks time and closes or opens jalousies according to it.
     */
    public Window getWindow() { return window; }

    /**
     * Method which checks time and closes or opens jalousies according to it.
     */
    public void checkWindows() {
        // day
        if (time > 6 && time < 22) {
            if (people.size() + pets.size() >= 2) window.open();
            window.getJalousie().liftUp();
        }
        // night
        else {
            window.close();
            window.getJalousie().pullDown();
        }
    }

    /**
     * Method which checks time and turns on or off radiators according to it.
     */
    public void checkTemperature() {
        // day
        if (time > 6 && time < 22) {
            if (temperature < 18) {
                radiator.turnOn();
            } else if (temperature > 22){
                radiator.turnOff();
            }
        }
        // night
        else {
            if (temperature < 16) {
                radiator.turnOn();
            } else if (temperature > 19){
                radiator.turnOff();
            }
        }
        if (radiator.canWork()) temperature += 2;
        if (window.getState() == Window.windowState.OPEN) temperature--;
        temperature--;
    }

    /**
     * Method which checks if there is somebody in the room. Sets motion sensors according to it.
     */
    public void checkMotionSensor() {
        if (people.isEmpty() && pets.isEmpty())
            motionSensor.setActive(false);
        else motionSensor.setActive(true);
    }

    /**
     * Method which finds out if people in this room are sleeping or not
     * @return boolean
     */
    public boolean peopleAwake() {
        for (Person p : people){
            if (!p.getSleeping()) return true;
        }
        return false;
    }

    /**
     * Method which checks time and jalousies state. Sets smart bulbs according to it.
     */
    public void checkLights() {
        if ((window.getJalousie().getState() == Jalousie.jalousieState.DOWN || time < 6  || time > 22) && motionSensor.isActive()) {
            if (peopleAwake()) smartBulb.turnOn();
            else smartBulb.turnOff();
        } else smartBulb.turnOff();
    }

    @Override
    public void update(int time) {
        this.time = time % 24;
        checkMotionSensor();
        checkWindows();
        checkLights();
        checkTemperature();
    }

    public Radiator getRadiator() {
        return radiator;
    }

    public int getTemperature(){
        return temperature;
    }
}
