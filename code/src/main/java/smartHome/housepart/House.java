package smartHome.housepart;

import smartHome.activitytool.Bicycle;
import smartHome.activitytool.Car;
import smartHome.activitytool.GameConsole;
import smartHome.device.*;
import smartHome.device.coffee.CoffeeMaker;
import smartHome.person.*;
import smartHome.pet.Pet;

import java.util.*;

public class House {

    private final List<HousePart> houseParts = new ArrayList<>();
    private final List<Person> people = new ArrayList<>();
    private final List<Pet> pets = new ArrayList<>();
    private final List<LawnSprinkler> lawnSprinklers = new ArrayList<>();
    private final List<Cooker> cookers = new ArrayList<>();
    private final List<Fridge> fridges = new ArrayList<>();
    private final List<MotionSensor> motionSensors = new ArrayList<>();
    private final List<Bicycle> bikes = new ArrayList<>();
    private final List<TV> TVs = new ArrayList<>();
    private final List<Microwave> microwaves = new ArrayList<>();
    private final List<SmartBulb> smartBulbs = new ArrayList<>();
    private final List<CoffeeMaker> coffeeMakers = new ArrayList<>();
    private final List<Radiator> radiators = new ArrayList<>();

    private Map<Skills, Boolean> fatherSkills = new HashMap<>();
    private Map<Skills, Boolean> motherSkills = new HashMap<>();
    private Map<Skills, Boolean> childSkills = new HashMap<>();

    private List<Car> cars = new ArrayList<>();
    private List<Bicycle> bicycles = new ArrayList<>();
    private List<GameConsole> gameConsoles = new ArrayList<>();

    private static House instance;

    private House() {}

    //singleton
    public static House getInstance() {
        if (instance == null)
            instance = new House();
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    public void addHousePart(HousePart housePart){
        this.houseParts.add(housePart);
    }

    public void addFamilyMembers(List<Person> persons) {
        this.people.addAll(persons);
    }

    /**
     * Method which finds the fastest path to requested house part.
     * @param path is a list of house parts
     * @param wantedHousePart is where a person wants to go
     * @return path
     */
    private List<HousePart> getBestPath(List<HousePart> path, HousePart wantedHousePart) {
        if (path.get(path.size() - 1).equals(wantedHousePart)) return path;
        final List<List<HousePart>> allPaths = new ArrayList<>();
        for (HousePart r : path.get(path.size() - 1).getNeighbourRooms()) {
            if (!path.contains(r)) {
                List<HousePart> newPath = new ArrayList<>(path);
                newPath.add(r);
                allPaths.add(newPath);
            }
        }

        int bestResult = 99;
        List<HousePart> bestPath = new ArrayList<>();
        for (List<HousePart> p : allPaths) {
            if (!path.containsAll(p)) {
                List<HousePart> resPath = getBestPath(p, wantedHousePart);
                if (resPath.size() < bestResult && resPath.contains(wantedHousePart))
                    bestPath = resPath;
            }
        }
        return bestPath;
    }

    public List<HousePart> getPath(Person p, HousePart wantedHousePart){
        List<HousePart> path = new ArrayList<>();
        path.add(p.getActualRoom());
        path = getBestPath(path, wantedHousePart);
        return path;
    }

    public void setPersonsSkills() {
        for (Person p : people) {
            if (p instanceof Father) {
                p.setSkillsMap(fatherSkills);
            } else if (p instanceof Mother) {
                p.setSkillsMap(motherSkills);
            } else if (p instanceof Child) {
                p.setSkillsMap(childSkills);
            }
        }
    }

    public void setActivityTools() {
        for (Person p : people) {
            if (p instanceof Father) {
                p.setCars(cars);
                p.setBicycles(List.of(bicycles.get(0)));
            } else if (p instanceof Mother) {
                p.setBicycles(List.of(bicycles.get(1)));
            } else if (p instanceof Child) {
                p.setGameConsoles(gameConsoles);
            }
        }
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public List<Bicycle> getBicycles() {
        return bicycles;
    }

    public void setBicycles(List<Bicycle> bicycles) {
        this.bicycles = bicycles;
    }

    public List<GameConsole> getGameConsoles() {
        return gameConsoles;
    }

    public void setGameConsoles(List<GameConsole> gameConsoles) {
        this.gameConsoles = gameConsoles;
    }

    public void addPets(List<Pet> pets) {
        this.pets.addAll(pets);
    }

    public void addCar(Car car) {
        this.cars.add(car);
    }
    public void addLawnSprinkler(LawnSprinkler lawnSprinkler) {
        this.lawnSprinklers.add(lawnSprinkler);
    }
    public void addFridge(Fridge fridge) {
        this.fridges.add(fridge);
    }
    public void addCooker(Cooker cooker) {
        this.cookers.add(cooker);
    }

    public void addMotionSensor(MotionSensor motionSensor) {
        this.motionSensors.add(motionSensor);
    }

    public void addBike(Bicycle bicycle) {
        this.bikes.add(bicycle);
    }

    public void addTV(TV tv) {
        this.TVs.add(tv);
    }

    public void addMicroWave(Microwave microwave) {
        this.microwaves.add(microwave);
    }

    public void addSmartBulb(SmartBulb smartBulb) {
        this.smartBulbs.add(smartBulb);
    }

    public void addCoffeeMaker(CoffeeMaker coffeeMaker) {
        this.coffeeMakers.add(coffeeMaker);
    }

    public void addRadiator(Radiator radiator) { this.radiators.add(radiator); }

    public void addGameConsole(GameConsole gameConsole) { gameConsoles.add(gameConsole); }

    public void setFatherSkills(Map<Skills, Boolean> map) { fatherSkills = map; }

    public void setMotherSkills(Map<Skills, Boolean> map) { motherSkills = map; }

    public void setChildSkills(Map<Skills, Boolean> map) { childSkills = map; }

    public List<HousePart> getHouseParts() {
        return houseParts;
    }

    public List<Person> getPeople() {
        return people;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public Map<Skills, Boolean> getFatherSkills() {
        return fatherSkills;
    }

    public Map<Skills, Boolean> getMotherSkills() {
        return motherSkills;
    }

    public Map<Skills, Boolean> getChildSkills() {
        return childSkills;
    }

    public List<Car> getCars() {
        return cars;
    }

    public List<LawnSprinkler> getLawnSprinklers() {
        return lawnSprinklers;
    }

    public List<Cooker> getCookers() {
        return cookers;
    }

    public List<Fridge> getFridges() {
        return fridges;
    }

    public List<MotionSensor> getMotionSensors() {
        return motionSensors;
    }

    public List<Bicycle> getBikes() {
        return bikes;
    }

    public List<TV> getTVs() {
        return TVs;
    }

    public List<Microwave> getMicrowaves() {
        return microwaves;
    }

    public List<SmartBulb> getSmartBulbs() {
        return smartBulbs;
    }

    public List<CoffeeMaker> getCoffeeMakers() {
        return coffeeMakers;
    }

    public List<Radiator> getRadiators() { return radiators; }

    public HousePart getHousePart(HousePartType type) {
        for (HousePart r : houseParts) {
            if (r.getType().equals(type))
                return r;
        }
        return null;
    }

    public static void clean(){
        instance = null;
    }

}
