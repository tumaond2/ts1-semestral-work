package smartHome.housepart;

public class Jalousie {

    public enum jalousieState {
        UP, DOWN
    }

    private jalousieState state;
    private Window window;

    public Jalousie() {
        state = jalousieState.UP;
    }

    public jalousieState getState() {
        return state;
    }

    public Window getWindow() {
        return window;
    }

    public void liftUp(){
        state = jalousieState.UP;
    }

    public void pullDown(){
        state = jalousieState.DOWN;
    }

    public void setWindow(Window window) { this.window = window; }
}
