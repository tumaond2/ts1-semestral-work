package smartHome.housepart;

public enum HousePartType {
    KITCHEN, LIVING_ROOM, GARAGE, DINNING_ROOM, KIDS_ROOM1, KIDS_ROOM2, HALL, GARDEN
}
