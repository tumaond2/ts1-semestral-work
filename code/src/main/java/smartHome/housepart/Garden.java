package smartHome.housepart;

import smartHome.device.LawnSprinkler;

public class Garden extends HousePart {

    private LawnSprinkler lawnSprinkler;

    public Garden(HousePartType type) {
        super(type);
        lawnSprinkler = new LawnSprinkler();
        lawnSprinkler.setHousePart(this);
        devices.add(lawnSprinkler);
        House.getInstance().addLawnSprinkler(lawnSprinkler);
    }

    @Override
    public void update(int time) {}

    public LawnSprinkler getLawnSprinkler() {
        return lawnSprinkler;
    }
}
