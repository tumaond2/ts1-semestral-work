package smartHome.housepart;

public class Window {

    public enum windowState{
        OPEN, CLOSED
    }

    private windowState state;
    private Jalousie jalousie = null;
    private HousePart housePart = null;

    public Window() {
        state = windowState.CLOSED;
    }

    public windowState getState() {
        return state;
    }

    public Jalousie getJalousie() { return jalousie; }

    public HousePart getHousePart() { return housePart; }

    public void setJalousie(Jalousie jalousie) { this.jalousie = jalousie; }

    public void setState(windowState state) {
        this.state = state;
    }

    public void setHousePart(HousePart housePart) { this.housePart = housePart; }

    public void open(){
        state = windowState.OPEN;
    }

    public void close(){
        state = windowState.CLOSED;
    }
}
