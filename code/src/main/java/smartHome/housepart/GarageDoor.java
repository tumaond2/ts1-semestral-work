package smartHome.housepart;

public class GarageDoor {

    public enum garageDoorState{
        OPEN, CLOSED
    }

    protected garageDoorState state;
    protected HousePart housePart;

    public GarageDoor() {
        state = garageDoorState.CLOSED;
    }

    public garageDoorState getState() {
        return state;
    }

    public void open(){
        state = garageDoorState.OPEN;
    }

    public void close(){
        state = garageDoorState.CLOSED;
    }

    public HousePart getHousePart() { return housePart; }

    public void setHousePart(HousePart housePart) { this.housePart = housePart; }
}
