package smartHome.housepart;

import smartHome.activitytool.ActivityTool;
import smartHome.device.Device;
import smartHome.device.MotionSensor;
import smartHome.device.SmartBulb;
import smartHome.interfaces.Observer;
import smartHome.interfaces.TimeControllerImpl;
import smartHome.person.Person;
import smartHome.pet.Pet;

import java.util.ArrayList;
import java.util.List;

public abstract class HousePart implements Observer {

    protected final List<HousePart> neighbourHouseParts = new ArrayList<>();
    protected List<Device> devices = new ArrayList<>();
    protected List<ActivityTool> activityTools = new ArrayList<>();
    protected List<Person> people = new ArrayList<>();
    protected List<Pet> pets = new ArrayList<>();
    protected HousePartType type;
    protected int time = 0;
    protected MotionSensor motionSensor;
    protected SmartBulb smartBulb;

    public HousePart(HousePartType type) {
        this.type = type;
        this.motionSensor = new MotionSensor();
        this.smartBulb = new SmartBulb();
        motionSensor.setHousePart(this);
        smartBulb.setHousePart(this);
        devices.add(motionSensor);
        devices.add(smartBulb);
        House.getInstance().addMotionSensor(motionSensor);
        House.getInstance().addSmartBulb(smartBulb);
        TimeControllerImpl.getInstance().attach(this);
    }

    public HousePartType getType() { return type; }

    public List<Device> getDevices() { return devices; }

    public List<ActivityTool> getActivityTools() { return activityTools; }

    public void addDevice(Device device){
        devices.add(device);
    }

    public void addActivityTool(ActivityTool tool) { activityTools.add(tool); }


    public void addPerson(Person person) {
        people.add(person);
    }

    public void removePerson(Person person) {
        people.remove(person);
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public void addNeighbourRoom(HousePart housePart) { neighbourHouseParts.add(housePart); }

    public List<HousePart> getNeighbourRooms() { return neighbourHouseParts; }

    @Override
    public abstract void update(int time);
}
