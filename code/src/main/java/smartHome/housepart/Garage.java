package smartHome.housepart;

public class Garage extends HousePart {

    private GarageDoor garageDoor;

    public Garage(HousePartType type) {
        super(type);
        this.garageDoor = new GarageDoor();
        garageDoor.setHousePart(this);
    }

    @Override
    public void update(int time) {

    }

    public GarageDoor getGarageDoor() {
        return garageDoor;
    }
}
