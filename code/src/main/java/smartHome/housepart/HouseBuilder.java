package smartHome.housepart;

import smartHome.activitytool.Bicycle;
import smartHome.activitytool.Car;
import smartHome.activitytool.GameConsole;
import smartHome.device.*;
import smartHome.device.coffee.*;
import smartHome.person.*;
import smartHome.pet.Cat;
import smartHome.pet.Dog;
import smartHome.pet.Pet;

import java.util.*;

//builder pattern
public class HouseBuilder {

    private House house = House.getInstance();

    /**
     * adds room of a concrete type to house.
     * @param type is type of the room.
     * @return housebuilder
     */
    public HouseBuilder addRoom(HousePartType type) {
        if (type != HousePartType.GARAGE && type != HousePartType.GARDEN) {
            final HousePart room = new Room(type);
            house.addHousePart(room);
        }
        return this;
    }


    /**
     * adds living room to house.
     * @return housebuilder
     */
    public HouseBuilder addLivingRoom() {
        final HousePart livingRoom = new Room(HousePartType.LIVING_ROOM);
        final TV tv = new TV();
        tv.setHousePart(livingRoom);
        livingRoom.addDevice(tv);
        house.addTV(tv);
        final GameConsole gameConsole = new GameConsole();
        gameConsole.setHousePart(livingRoom);
        livingRoom.addActivityTool(gameConsole);
        house.addGameConsole(gameConsole);
        house.addHousePart(livingRoom);
        return this;
    }

    /**
     * adds kitchen to house.
     * @return housebuilder
     */
    public HouseBuilder addKitchen() {
        final HousePart kitchen = new Room(HousePartType.KITCHEN);
        final CoffeeMaker lungoMaker = new LungoMaker();
        lungoMaker.setHousePart(kitchen);
        kitchen.addDevice(lungoMaker);
        house.addCoffeeMaker(lungoMaker);
        final CoffeeMaker latteMaker = new LatteMaker();
        latteMaker.setHousePart(kitchen);
        kitchen.addDevice(latteMaker);
        house.addCoffeeMaker(latteMaker);
        CoffeeMaker turekMaker = new TurekMaker();
        turekMaker.setHousePart(kitchen);
        kitchen.addDevice(turekMaker);
        house.addCoffeeMaker(turekMaker);
        final Fridge fridge = new Fridge();
        fridge.setHousePart(kitchen);
        kitchen.addDevice(fridge);
        house.addFridge(fridge);
        final Microwave microwave = new Microwave();
        microwave.setHousePart(kitchen);
        kitchen.addDevice(microwave);
        house.addMicroWave(microwave);
        final Cooker cooker = new Cooker();
        cooker.setHousePart(kitchen);
        kitchen.addDevice(cooker);
        house.addCooker(cooker);
        house.addHousePart(kitchen);
        return this;
    }

    /**
     * adds garage to house.
     * @return housebuilder
     */
    public HouseBuilder addGarage() {
        final HousePart garage = new Garage(HousePartType.GARAGE);
        house.addHousePart(garage);
        return this;
    }

    /**
     * adds garden to house.
     * @return housebuilder
     */
    public HouseBuilder addGarden() {
        final HousePart garden = new Garden(HousePartType.GARDEN);
        house.addHousePart(garden);
        return this;
    }

    /**
     * Sets neighbours of an incoming room.
     * @return housebuilder
     */
    public HouseBuilder addNeighbourHouseParts(HousePartType room, HousePartType[] neighbours) {
        for (HousePartType n : neighbours) {
            house.getHousePart(room).addNeighbourRoom(house.getHousePart(n));
        }
        return this;
    }

    /**
     * Method which encapsulates adding rooms to house.
     * @return housebuilder
     */
    public HouseBuilder addAllHouseParts1() {
        addKitchen();
        addLivingRoom();
        addRoom(HousePartType.DINNING_ROOM);
        addRoom(HousePartType.KIDS_ROOM1);
        addRoom(HousePartType.HALL);
        addGarage();
        addGarden();
        addNeighbourHouseParts(HousePartType.KITCHEN, new HousePartType[]{HousePartType.LIVING_ROOM});
        addNeighbourHouseParts(HousePartType.LIVING_ROOM, new HousePartType[]{HousePartType.KITCHEN, HousePartType.HALL});
        addNeighbourHouseParts(HousePartType.DINNING_ROOM, new HousePartType[]{HousePartType.HALL});
        addNeighbourHouseParts(HousePartType.KIDS_ROOM1, new HousePartType[]{HousePartType.HALL});
        addNeighbourHouseParts(HousePartType.HALL, new HousePartType[]{HousePartType.LIVING_ROOM, HousePartType.DINNING_ROOM, HousePartType.KIDS_ROOM1, HousePartType.GARAGE});
        addNeighbourHouseParts(HousePartType.GARAGE, new HousePartType[]{HousePartType.HALL, HousePartType.GARDEN});
        addNeighbourHouseParts(HousePartType.GARDEN, new HousePartType[]{HousePartType.GARAGE});

        return this;
    }

    public HouseBuilder addAllHouseParts2(){
        addKitchen();
        addLivingRoom();
        addRoom(HousePartType.DINNING_ROOM);
        addRoom(HousePartType.KIDS_ROOM1);
        addRoom(HousePartType.KIDS_ROOM2);
        addRoom(HousePartType.HALL);
        addGarage();
        addGarden();
        addNeighbourHouseParts(HousePartType.KITCHEN, new HousePartType[]{HousePartType.DINNING_ROOM});
        addNeighbourHouseParts(HousePartType.LIVING_ROOM, new HousePartType[]{HousePartType.DINNING_ROOM});
        addNeighbourHouseParts(HousePartType.DINNING_ROOM, new HousePartType[]{HousePartType.HALL, HousePartType.KITCHEN, HousePartType.LIVING_ROOM});
        addNeighbourHouseParts(HousePartType.KIDS_ROOM1, new HousePartType[]{HousePartType.HALL});
        addNeighbourHouseParts(HousePartType.KIDS_ROOM2, new HousePartType[]{HousePartType.HALL});
        addNeighbourHouseParts(HousePartType.HALL, new HousePartType[]{HousePartType.DINNING_ROOM, HousePartType.KIDS_ROOM1, HousePartType.KIDS_ROOM2, HousePartType.GARAGE});
        addNeighbourHouseParts(HousePartType.GARAGE, new HousePartType[]{HousePartType.HALL, HousePartType.GARDEN});
        addNeighbourHouseParts(HousePartType.GARDEN, new HousePartType[]{HousePartType.GARAGE});

        return this;
    }

    /**
     * Method which encapsulates adding 3 persons to house.
     * @return housebuilder
     */
    public HouseBuilder addFamilyMembers1() {
        final Person father = new Father("Darth Vader", house.getHousePart(HousePartType.DINNING_ROOM));
        final Person mother = new Mother("Padme", house.getHousePart(HousePartType.DINNING_ROOM));
        final Person child = new Child("Luke", house.getHousePart(HousePartType.KIDS_ROOM1));
        house.addFamilyMembers(List.of(father, mother, child));
        final HousePart hall = house.getHousePart(HousePartType.HALL);
        if (hall != null) {
            hall.addPerson(father);
            hall.addPerson(mother);
            hall.addPerson(child);
            father.setActualRoom(hall);
            mother.setActualRoom(hall);
            child.setActualRoom(hall);
        }
        return this;
    }


    /**
     * Method which encapsulates adding 6 persons to house.
     * @return housebuilder
     */
    public HouseBuilder addFamilyMembers2() {
        final Person father = new Father("Darth Vader", house.getHousePart(HousePartType.DINNING_ROOM));
        final Person mother = new Mother("Padme", house.getHousePart(HousePartType.DINNING_ROOM));
        final Person child1 = new Child("Luke", house.getHousePart(HousePartType.KIDS_ROOM1));
        final Person child2 = new Child("Obi Wan Kenobi", house.getHousePart(HousePartType.KIDS_ROOM1));
        final Person child3 = new Child("Lea", house.getHousePart(HousePartType.KIDS_ROOM2));
        final Person child4 = new Child("R2-D2", house.getHousePart(HousePartType.KIDS_ROOM2));


        house.addFamilyMembers(List.of(father, mother, child1, child2, child3, child4));
        final HousePart hall = house.getHousePart(HousePartType.HALL);
        if (hall != null) {
            hall.addPerson(father);
            hall.addPerson(mother);
            hall.addPerson(child1);
            hall.addPerson(child2);
            hall.addPerson(child3);
            hall.addPerson(child4);
            father.setActualRoom(hall);
            mother.setActualRoom(hall);
            child1.setActualRoom(hall);
            child2.setActualRoom(hall);
            child3.setActualRoom(hall);
            child4.setActualRoom(hall);
        }
        return this;
    }

    /**
     * Method which encapsulates adding pets to house.
     * @return housebuilder
     */
    public HouseBuilder addPets1() {
        final HousePart garden = house.getHousePart(HousePartType.GARDEN);
        final Pet dog = new Dog("Ciko");
        final Pet cat1 = new Cat("Mikes");
        final Pet cat2 = new Cat("Mourek");
        house.addPets(List.of(dog, cat1, cat2));
        if (garden != null) {
            garden.addPet(dog);
            garden.addPet(cat1);
            garden.addPet(cat2);
        }
        return this;
    }

    public HouseBuilder addPets2() {
        final HousePart garden = house.getHousePart(HousePartType.GARDEN);
        final Pet dog1 = new Dog("Ciko");
        final Pet dog2 = new Dog("Igor");
        final Pet cat = new Cat("Mourek");
        house.addPets(List.of(dog1, dog2, cat));
        if (garden != null) {
            garden.addPet(dog1);
            garden.addPet(dog2);
            garden.addPet(cat);
        }
        return this;
    }

    //IF YOU DO NOT WANT TO USE PRE-DEFINED CONFIGURATION
    public HouseBuilder addCooker() {
        house.addCooker(new Cooker());
        return this;
    }

    public HouseBuilder addFridge() {
        house.addFridge(new Fridge());
        return this;
    }

    public HouseBuilder addLawnSprinkler() {
        house.addLawnSprinkler(new LawnSprinkler());
        return this;
    }

    public HouseBuilder addMicrowave() {
        house.addMicroWave(new Microwave());
        return this;
    }


    public HouseBuilder addSmartBulb() {
        house.addSmartBulb(new SmartBulb());
        return this;
    }

    public HouseBuilder addTV() {
        house.addTV(new TV());
        return this;
    }

    /**
     * Sets skills for each type of person.
     * @return housebuilder
     */
    public HouseBuilder setPersonsSkills() {
        final Map<Skills, Boolean> fatherSkills = new HashMap<>();
        final Map<Skills, Boolean> motherSkills = new HashMap<>();
        final Map<Skills, Boolean> childSkills = new HashMap<>();

        fatherSkills.put(Skills.PLAY_WITH_PET, true);
        fatherSkills.put(Skills.FEED_PET, false);
        fatherSkills.put(Skills.REPAIR_DEVICE, true);
        fatherSkills.put(Skills.MAKE_COFFEE, true);
        fatherSkills.put(Skills.FILL_COFFEE, true);
        fatherSkills.put(Skills.FILL_WATER, true);
        fatherSkills.put(Skills.FILL_MILK, true);
        fatherSkills.put(Skills.CLEAN_COFFEE_MAKER, true);
        fatherSkills.put(Skills.RIDE_BIKE, true);
        fatherSkills.put(Skills.DRIVE_CAR, true);
        fatherSkills.put(Skills.PREPARE_MEAL, false);
        fatherSkills.put(Skills.HEAT_FOOD, true);
        fatherSkills.put(Skills.PLAY_CONSOLE, true);
        fatherSkills.put(Skills.EAT, true);
        fatherSkills.put(Skills.WATCH_TV, true);
        fatherSkills.put(Skills.MAKE_DRINK, true);
        fatherSkills.put(Skills.GO_BUY_FOOD, true);

        motherSkills.put(Skills.PLAY_WITH_PET, true);
        motherSkills.put(Skills.FEED_PET, true);
        motherSkills.put(Skills.REPAIR_DEVICE, false);
        motherSkills.put(Skills.MAKE_COFFEE, true);
        motherSkills.put(Skills.FILL_COFFEE, true);
        motherSkills.put(Skills.FILL_WATER, true);
        motherSkills.put(Skills.FILL_MILK, true);
        motherSkills.put(Skills.CLEAN_COFFEE_MAKER, true);
        motherSkills.put(Skills.RIDE_BIKE, true);
        motherSkills.put(Skills.DRIVE_CAR, false);
        motherSkills.put(Skills.PREPARE_MEAL, true);
        motherSkills.put(Skills.HEAT_FOOD, true);
        motherSkills.put(Skills.PLAY_CONSOLE, false);
        motherSkills.put(Skills.EAT, true);
        motherSkills.put(Skills.WATCH_TV, true);
        motherSkills.put(Skills.MAKE_DRINK, true);
        motherSkills.put(Skills.GO_BUY_FOOD, true);

        childSkills.put(Skills.PLAY_WITH_PET, true);
        childSkills.put(Skills.FEED_PET, true);
        childSkills.put(Skills.REPAIR_DEVICE, false);
        childSkills.put(Skills.MAKE_COFFEE, true);
        childSkills.put(Skills.FILL_COFFEE, true);
        childSkills.put(Skills.FILL_WATER, true);
        childSkills.put(Skills.FILL_MILK, true);
        childSkills.put(Skills.RIDE_BIKE, true);
        childSkills.put(Skills.CLEAN_COFFEE_MAKER, false);
        childSkills.put(Skills.DRIVE_CAR, false);
        childSkills.put(Skills.PREPARE_MEAL, false);
        childSkills.put(Skills.HEAT_FOOD, true);
        childSkills.put(Skills.PLAY_CONSOLE, true);
        childSkills.put(Skills.EAT, true);
        childSkills.put(Skills.WATCH_TV, true);
        childSkills.put(Skills.MAKE_DRINK, true);
        childSkills.put(Skills.GO_BUY_FOOD, true);


        house.setFatherSkills(fatherSkills);
        house.setMotherSkills(motherSkills);
        house.setChildSkills(childSkills);
        house.setPersonsSkills();
        return this;
    }

    /**
     * Method which encapsulates adding activity tools to house.
     * @return housebuilder
     */
    public HouseBuilder setActivityTools1() {
        final Car car = new Car();
        final Bicycle b1 = new Bicycle();
        final Bicycle b2 = new Bicycle();
        final GameConsole gc1 = new GameConsole();
        final GameConsole gc2 = new GameConsole();


        house.setCars(List.of(car));
        house.setBicycles(List.of(b1, b2));
        house.setGameConsoles(List.of(gc1, gc2));

        house.setActivityTools();
        return this;
    }

    public HouseBuilder setActivityTools2() {
        final Car car1 = new Car();
        final Car car2 = new Car();
        final Bicycle b1 = new Bicycle();
        final Bicycle b2 = new Bicycle();
        final Bicycle b3 = new Bicycle();
        final Bicycle b4 = new Bicycle();
        final GameConsole gc1 = new GameConsole();
        final GameConsole gc2 = new GameConsole();


        house.setCars(List.of(car1, car2));
        house.setBicycles(List.of(b1, b2, b3, b4));
        house.setGameConsoles(List.of(gc1, gc2));

        house.setActivityTools();
        return this;
    }

    /**
     * @return house
     */
    public House getResult() {
        return house;
    }

}
