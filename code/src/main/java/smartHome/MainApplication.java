package smartHome;

import smartHome.housepart.*;
import smartHome.interfaces.TimeControllerImpl;
import smartHome.interfaces.TimeController;
import smartHome.report.Report;

import java.util.List;


public class MainApplication {

    public static void main(String[] args) {
        final HouseBuilder builder = new HouseBuilder();
        final TimeController time = TimeControllerImpl.getInstance();
        final Report report = Report.getInstance();
        final House house1 = builder                                                                             //variable house is here for better understanding what is being created
                                .addAllHouseParts1()
                                .addFamilyMembers1()
                                .addPets1()
                                .setPersonsSkills()
                                .setActivityTools1()
                                .getResult();

//        final House house2 = builder                                                                             //variable house is here for better understanding what is being created
//                .addAllHouseParts2()
//                .addFamilyMembers2()
//                .addPets2()
//                .setPersonsSkills()
//                .setActivityTools2()
//                .getResult();

        report.generateHouseConfigurationReport();
        time.timePeriod();
        report.generateEventReport(1, 38);
    }
}
