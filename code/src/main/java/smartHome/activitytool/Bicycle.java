package smartHome.activitytool;

import smartHome.person.Person;

public class Bicycle extends ActivityTool {

    private int currentTime;
    private int timeFrom;
    private Person rider;

    /**
     * Method which simulates person riding a bike.
     * @param person is a current user of this bicycle.
     */
    public void ride(Person person) {
        isOccupied = true;
        timeFrom = currentTime;
        rider = person;
        System.out.println(person.getName() + " is riding a bike");
    }

    /**
     * Checks if a person still uses this bicycle.
     */
    public void checkState() {
        if (currentTime == timeFrom + 1) {
            isOccupied = false;
            rider.setWaitingToRideBike(false);
        }
    }

    @Override
    public void update(int time) {
        currentTime = time;
        checkState();
    }
}
