package smartHome.activitytool;

import smartHome.housepart.HousePart;
import smartHome.person.Person;

public class GameConsole extends ActivityTool {

    private int currentTime;
    private int timeFrom;
    private HousePart housePart;

    public HousePart getHousePart() { return housePart; }

    public void setHousePart(HousePart housePart) { this.housePart = housePart; }

    /**
     * Method which simulates person playing a game.
     * @param person is a current user of this game console.
     */
    public void play(Person person) {
        isOccupied = true;
        timeFrom = currentTime;
        System.out.println(person.getName() + " is playing a game");
    }

    /**
     * Checks if a person still uses this console.
     */
    public void checkState() {
        if (currentTime == timeFrom + 2)
            isOccupied = false;
    }

    @Override
    public void update(int time) {
        currentTime = time;
        checkState();
    }
}
