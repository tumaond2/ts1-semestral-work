package smartHome.activitytool;

import smartHome.interfaces.Observer;

public abstract class ActivityTool implements Observer {

    /**
     * Variable which shows if a tool is being used by a person.
     */
    protected boolean isOccupied = false;

    /**
     * Method which is called by TimeControllerImpl every time tick.
     * Controls whole lifecycle of an activity desire.
     * @param time is a current time tick.
     */
    @Override
    public abstract void update(int time);

    public boolean isOccupied() {
        return isOccupied;
    }
}
