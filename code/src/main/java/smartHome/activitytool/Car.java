package smartHome.activitytool;

import smartHome.person.Person;

public class Car extends ActivityTool {

    private int currentTime;
    private int timeFrom;

    /**
     * Method which simulates person driving a car.
     * @param person is a current user of this car.
     */
    public void drive(Person person) {
        isOccupied = true;
        timeFrom = currentTime;
        System.out.println(person.getName() + " is driving a car");
    }

    /**
     * Checks if a person still uses this car.
     */
    public void checkState() {
        if (currentTime == timeFrom + 2)
            isOccupied = false;
    }

    @Override
    public void update(int time) {
        currentTime = time;
        checkState();
    }
}
