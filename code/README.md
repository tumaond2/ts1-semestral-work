_**SMART HOME - simulace chytrého domu**_

_Autoři:_ Robin Vávra, Vít Šesták

_Popis:_
V mainu se nastaví celý dům pomocí builder patternu. 
Rozložení domu je popsáno v automaticky vygenerovaném souboru _HouseConfigurationReport.txt_.
Akce osob jsou řízeny eventy, které se v průběhu simulace vkládají do EventQueue a řadí se podle priorit.
Osoby si do EventQueue mohou přidávat eventy podle jejich vlastních potřeb.
V domě se nachází nejrůznější zařízení sloužící k uspokojení potřeb osob.
Pojem o čase si osoby i zařízení udržují pomoci metody update, která se volá v každém "taktu" z rozhraní TimeControllerImpl.
Zařízení se však mohou rozbít nebo může chybět nějaká jejich složka nezbytná ke správnému fungovaní, takže před použitím musí osoba tento problém vyřešit.
Eventy generované zařízeními, zvířaty či volnočasovými nástroji mohou byt adresované přímo konkrétnímu člověku či neurčité osobě.
Dále se v domě nachází nástroje pro volnočasové aktivity osob.
Osoby mají na starost také několik zvířat, o které se musí starat.
Na konci simulace se vygeneruje soubor _EventReport.txt_, kde jsou uloženy všechny přidané *eventy v určitém čase*. (čas si zvolí sám uživatel v mainu)



Podařilo se nám tedy vytvořit simulaci chytrého domu, kde zařízení a lidí mají ví, jaký je zrovna čas, zařízení mají svůj stav, který v každém hodinovém tiku kontrolují, generují eventy a vykonávají procedury (např. udělej latte). Lidé si hlídají své potřeby a v případě, že nějaká z nich dosáhne určité úrovně, generují další eventy, přičemž použijí metodu, která najde první volné zařízení, které je schopno jejich potřebu uspokojit. Nechtěli bychom opomenout ani zvířata, která si spí ,kdy chtějí, generují eventy, když potřebují trochu lidské něhy či když mají touhu po potravě.

Při implementaci jsme taktéž hojně využili generické typy. Pomohlo nám taktéž přidání parametru _Person executor_ do kontruktoru eventu, protože nám to umožnilo, aby spolu související eventy byly přiřazeny témuž člověku, možný příklad užití: Yoda si chce udělat kávu -> 1.event udělat kávu, 2. event vyčistit kávovar, 3. event doplnit mléko, 4. event doplnit vodu.. Všechny tyto eventy bude díky atributu _executor_ řešit Yoda.

Na závěr bychom chtěli vyzdvihnout pohyb osob po domě pomocí _hledání nejkratší cesty_ a možnost _generování eventů za libovolný časový interval_.

_Použité patterny:_

Builder<br/>
Observer<br/>
Template method<br/>
Singleton<br/>
Factory<br/>
Visitor<br/>
State machine<br/>